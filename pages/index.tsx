import * as React from 'react';

import axios from 'axios';
import { NextPage } from 'next';

import { SearchApiResult, WidgetProps } from '../src/Widget/model';
import { Widget } from '../src/Widget';

const Home: NextPage<WidgetProps> = (props) => {

  return <Widget {...props} />;
};

Home.getInitialProps = async () => {
  const { data } = await axios.get<SearchApiResult>(
    'https://search-api.fie.future.net.uk/widget.php?id=review&model_name=xbox_one_s&area=GB', 
    {
      timeout: 1000
    }
  );

  return { searchApiResult: data };
}

export default Home;