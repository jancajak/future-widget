import React from 'react'
import { IntlProvider } from 'react-intl'

import { AppProps } from 'next/app'
import { createGlobalStyle, ThemeProvider } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  html,body {
    padding: 0;
    margin: 0;
    font-family: 'Roboto', sans-serif;
    font-size: 16px;
  }

  a {
    color: inherit;
    text-decoration: none;
  }

  * {
    box-sizing: border-box;
  }

`

const App: React.FC<AppProps> = ({ Component, pageProps }) =>{
  return (
    // In real scenario we would fetch messages and locale from api
    <IntlProvider locale="en-EN" messages={{}}>
      <GlobalStyle />
      <ThemeProvider theme={{}}>
        <Component {...pageProps} />
      </ThemeProvider>
    </IntlProvider>
  )
}

export default App;