import { defineMessages } from 'react-intl';

export const widgetMessages = defineMessages({
  productName: {
    id: 'widget.product.name',
    defaultMessage: 'Name',
  },
  productPrice: {
    id: 'widget.product.price',
    defaultMessage: 'Price',
  },
  merchant: {
    id: 'widget.merchant',
    defaultMessage: 'Merchant',
  },
  model: {
    id: 'widget.product.model',
    defaultMessage: 'Model',
  },
  inStock: {
    id: 'widget.product.in.stock',
    defaultMessage: 'In Stock',
  },
  link: {
    id: 'widget.product.link',
    defaultMessage: 'Link',
  },
  explore: {
    id: 'widget.product.explore',
    defaultMessage: 'Explore',
  }
})