export interface WidgetProps {
  searchApiResult: SearchApiResult;
}

export interface SearchApiResult {
  widget: Widget;
}

export interface Widget {
  data: WidgetData;
}

export interface WidgetData {
  offers: WidgetDataOffers[];
}

export interface WidgetDataOffers {
  image: string;
  id: number;
  merchant: WidgetDataOffersMerchant;
  offer: WidgetDataOffersOffer;
}

export interface WidgetDataOffersMerchant {
  id: number;
  logo_url: string;
  url: string;
}

export interface WidgetDataOffersOffer {
  display_name: string;
  currency_symbol: string;
  in_stock: boolean;
  price: string;
  link: string;
  merchant_link_text: string;
}