import '@fortawesome/fontawesome-free/css/all.css';

import * as React from "react";
import { FormattedMessage } from "react-intl";

import Head from "next/head";
import Image from "next/image";

import { WidgetProps } from "./model";
import {
  MerchantImageWrapper,
  PriceWrapper,
  ProductImageWrapper,
  RowWrapper,
  StyledCell,
  StyledHeader,
  StyledHeaderWrapper,
  StyledOfferLink,
  WidgetWrapper,
} from "./styled";
import { widgetMessages } from "./messages";

export const Widget: React.FC<WidgetProps> = ({ searchApiResult }) => {
  if(!searchApiResult?.widget?.data?.offers) {
    return null;
  }

  return (
    <WidgetWrapper>
      <Head>
        <title>Future Widget</title>
        <meta name="description" content="Sample next.js app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
        <StyledHeaderWrapper>
          <StyledHeader heigth={132}>
            <FormattedMessage {...widgetMessages.model} />
          </StyledHeader>
          <StyledHeader>
            <FormattedMessage {...widgetMessages.productName} />
          </StyledHeader>
          <StyledHeader>
            <FormattedMessage {...widgetMessages.productPrice} />
          </StyledHeader>
          <StyledHeader>
            <FormattedMessage {...widgetMessages.inStock} />
          </StyledHeader>
          <StyledHeader>
            <FormattedMessage {...widgetMessages.explore} />
          </StyledHeader>
          <StyledHeader>
            <FormattedMessage {...widgetMessages.merchant} />
          </StyledHeader>
        </StyledHeaderWrapper>
        {searchApiResult.widget.data.offers.map(
          ({ id, offer, merchant, image }) => (
            <RowWrapper key={`row-${id}`}>
              <StyledCell heigth={132}>
                <ProductImageWrapper>
                  <Image
                    alt={`${offer.display_name}-image`}
                    src={image}
                    layout="fill"
                    objectFit="contain"
                  />
                </ProductImageWrapper>
              </StyledCell>
              <StyledCell>
                <span>{offer.display_name}</span>
              </StyledCell>
              <StyledCell>
                <PriceWrapper>
                  <span
                    dangerouslySetInnerHTML={{ __html: offer.currency_symbol }}
                  />
                  {offer.price}
                </PriceWrapper>
              </StyledCell>
              <StyledCell>
                <i className={`fas fa-${offer.in_stock ? 'check' : 'times'}`}/>
              </StyledCell>
              <StyledCell>
                <StyledOfferLink href={offer.link}>{offer.merchant_link_text}</StyledOfferLink>
              </StyledCell>
              <StyledCell>
                <span>
                  {merchant.logo_url && (
                    <a href={merchant.url}>
                      <MerchantImageWrapper>
                        <Image
                          layout="fill"
                          objectFit="contain"
                          src={merchant.logo_url}
                          alt="merchant-logo-url"
                        />
                      </MerchantImageWrapper>
                    </a>
                  )}
                  </span>
              </StyledCell>
            </RowWrapper>
          )
        )}
    </WidgetWrapper>
  );
};
