import styled from 'styled-components';

export const RowWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
`;

export const WidgetWrapper = styled.div`
  padding: 2rem;
  display: flex;
  flex-wrap: wrap;
  margin-top: -1px;
  margin-left: -1px;
`;

export const StyledHeader = styled.div<{ heigth?: number }>`
  height: ${({ heigth }) => heigth ?? 32}px;
  min-width: 10rem;
  display: flex;
  justify-content: center;
  align-items: center;
  font-weight: bold;
  background-color: #f2f2f2;
  outline: 1px solid #b3b3b3;
  margin-top: 1px;
  margin-left: 1px;
  padding: 2rem;
`;

export const StyledCell = styled.div<{ heigth?: number }>`
  height: ${({ heigth }) => heigth ?? 32}px;
  min-width: 20rem;
  font-weight: 300;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 2rem;
  outline: 1px solid #b3b3b3;
  margin-top: 1px;
  margin-left: 1px;
`;

export const StyledHeaderWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const StyledOfferLink = styled.a`
  color: #1976d2;
  &:hover {
    text-decoration: underline;
  }
`;

export const MerchantImageWrapper = styled.div`
  position: relative; 
  width: 150px; 
  padding-bottom: 20%; 
`;

export const ProductImageWrapper = styled.div`
  position: relative; 
  width: 200px; 
  padding-bottom: 20%; 
`;

export const PriceWrapper = styled.div`
  font-size: 1.3rem;
  font-weight: bold;
`;
