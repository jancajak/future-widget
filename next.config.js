module.exports = (_, { defaultConfig }) => ({
  ...defaultConfig,
  images: {
    ...defaultConfig.images,
    domains: [
      'mos.fie.futurecdn.net'
    ],
  },
});
